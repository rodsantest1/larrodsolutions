﻿
declare @selectedLocation nvarchar(25) = 'Pierce'

--min and max averages over all positions
declare @avgPayGrades table (
posnam nvarchar(255),
poshrs int,
cntgrd int,
minavg decimal(18,4),
maxavg decimal(18,4)
)

insert @avgPayGrades
select posnam,poshrs,posgrd, 
AVG(minhrs) minavg, AVG(maxhrs) maxavg
from CustomerImport
where locnam <> @selectedLocation
group by posnam, posgrd, poshrs

declare @proposedPositionGrades table (
locnam nvarchar(255),
posnam nvarchar(255),
ppdgrd int
)

insert @proposedPositionGrades
select locnam, posnam, posgrd from CustomerImport

;with cte as (
select p.locnam, p.posnam, g.poshrs, t.posgrd CurrentPayGrade, x.PayGrade ProposedPayGrade, 
	g.minavg BenchMarkMinimumAverage, 
	g.maxavg BenchmarkMaximumAverage, 
	HourlyMinimum ProposedHourlyMinimumRate, 
	HourlyMidPoint ProposedHourlyMidpointRate, 
	HourlyMaximum ProposedHourlyMaximumRate, 
	(HourlyMinimum * t.poshrs)  ProposedSalaryMinimumRate, 
	(HourlyMidPoint * t.poshrs) ProposedSalaryMidpointRate, 
	(HourlyMaximum * t.poshrs) ProposedSalaryMaximumRate, 
	t.minhrs CurrentMinimumHourlyRate,
	t.maxhrs CurrentMaximumHourlyRate
from @proposedPositionGrades p
join @avgPayGrades g
on p.posnam = g.posnam 
join ProposedMatrix x
on p.ppdgrd = x.PayGrade
join CustomerImport t
on t.locnam = p.locnam
and t.posnam = p.posnam
), cte2 as (
select posnam,poshrs,
	CurrentPayGrade,
	ProposedPayGrade,
	BenchMarkMinimumAverage,
	BenchmarkMaximumAverage,
	CurrentMinimumHourlyRate,
	ProposedHourlyMinimumRate,
	CurrentMaximumHourlyRate,
	(CurrentMinimumHourlyRate * poshrs) CurrentSalaryMinimumRate, 
	ProposedSalaryMinimumRate,
	format(((CurrentMinimumHourlyRate/BenchMarkMinimumAverage)-1),'p') '% Diff',
	(BenchMarkMinimumAverage * poshrs) BenchmarkMinimumSalary
from cte
where locnam = @selectedLocation
) 
select @selectedLocation 'County', *, 
(ProposedSalaryMinimumRate - BenchmarkMinimumSalary) IncreaseFromCurrent,
format(((ProposedSalaryMinimumRate / BenchmarkMinimumSalary)-1),'p') '% Deviation'
from cte2
order by posnam