﻿BACKUP DATABASE [EECdb] TO  DISK = N'C:\Users\rodneysantiago\source\repos\larrodsolutions\Solutions\WebApplication1UserInterfacePrototypeSol\EECdb\EECdb.bak' WITH NOFORMAT, INIT,  NAME = N'EECdb-Full Database Backup', SKIP, NOREWIND, NOUNLOAD,  STATS = 10
GO

USE [master]
ALTER DATABASE [EECdb] SET SINGLE_USER WITH ROLLBACK IMMEDIATE
RESTORE DATABASE [EECdb] FROM  DISK = N'C:\Users\rodneysantiago\source\repos\larrodsolutions\Solutions\WebApplication1UserInterfacePrototypeSol\EECdb\EECdb.bak' WITH  FILE = 1,  NOUNLOAD,  REPLACE,  STATS = 5
ALTER DATABASE [EECdb] SET MULTI_USER

GO