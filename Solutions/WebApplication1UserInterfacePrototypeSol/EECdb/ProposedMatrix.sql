﻿
DECLARE @startingSalary DECIMAL(18,4);
DECLARE @PartTimeHoursMaxWeek INT = 29
DECLARE @PartTimeHoursMaxYear int = 1508
DECLARE @MinimumWage decimal(5,2) = 7.25
DECLARE @YearlyHours INT = 2080
DECLARE @PartTimeHours INT = 1508
DECLARE @WeeksInYear INT = 52
DECLARE @MidpointMultiplier DECIMAL(5,2) = 1.3
DECLARE @MaximumMultiplier DECIMAL(5,2) = 1.23

SET @startingSalary = 15

DECLARE @salaryMatrix TABLE
(
[PayGrade] [INT],
[PercentageIncrease] [DECIMAL](18,4),
[PercentageInc] [DECIMAL](5,2),
[HourlyMinimum] [DECIMAL](18,4),
[HourlyMidPoint] [DECIMAL](18,4),
[HourlyMaximum] [DECIMAL](18,4),
[SalaryMinimum] [DECIMAL](18,4),
[SalaryMidPoint] [DECIMAL](18,4),
[SalaryMaximum] [DECIMAL](18,4)
)

DECLARE @SalaryMinimumBase DECIMAL(18,4)
DECLARE @SalaryMidpointBase DECIMAL(18,4)
DECLARE @SalaryMaximumBase DECIMAL(18,4)
DECLARE @HourlyMinimumBase DECIMAL(18,4)
DECLARE @HourlyMidpointBase DECIMAL(18,4)
DECLARE @HourlyMaximumBase DECIMAL(18,4)

SET @SalaryMinimumBase = @startingSalary * @YearlyHours
SET @SalaryMidpointBase = @SalaryMinimumBase * @MidpointMultiplier
SET @SalaryMaximumBase = @SalaryMidpointBase * @MaximumMultiplier
SET @HourlyMinimumBase = @SalaryMinimumBase / @YearlyHours
SET @HourlyMidpointBase = @SalaryMidpointBase / @YearlyHours
SET @HourlyMaximumBase = @SalaryMaximumBase / @YearlyHours

SELECT
@SalaryMinimumBase  SalaryMinimumBase
,@SalaryMidpointBase SalaryMidpointBase
,@SalaryMaximumBase SalaryMaximumBase
,@HourlyMinimumBase HourlyMinimumBase
,@HourlyMidpointBase HourlyMidpointBase
,@HourlyMaximumBase HourlyMaximumBase

INSERT INTO @salaryMatrix (PayGrade, PercentageIncrease, PercentageInc)
SELECT PayGrade, ((100 + PercentageIncrease) / 100), PercentageIncrease FROM PayGradePercentageIncrease

update @salaryMatrix
set 
HourlyMinimum = @HourlyMinimumBase,
HourlyMidpoint = @HourlyMidpointBase,
HourlyMaximum = @HourlyMaximumBase,
SalaryMinimum = @SalaryMinimumBase,
SalaryMidpoint = @SalaryMidpointBase,
SalaryMaximum = @SalaryMaximumBase

;WITH cte AS (
	select *, min(SalaryMinimum) over() * exp(sum(log(isnull(nullif(PercentageIncrease, 0), 1))) over(order by PayGrade rows unbounded preceding)) nextSalaryMinimum from @salaryMatrix 
)
UPDATE cte SET SalaryMinimum = nextSalaryMinimum

update @salaryMatrix set SalaryMidPoint = SalaryMinimum * @MidpointMultiplier
update @salaryMatrix set SalaryMaximum = SalaryMidPoint * @MaximumMultiplier
update @salaryMatrix set HourlyMinimum = SalaryMinimum / @YearlyHours
update @salaryMatrix set HourlyMidPoint = SalaryMidPoint / @YearlyHours
update @salaryMatrix set HourlyMaximum = SalaryMaximum / @YearlyHours

--insert @salaryMatrix (PayGrade) select 'PT'
--update @salaryMatrix set HourlyMinimum = @MinimumWage where PayGrade = 'PT'
--update @salaryMatrix set SalaryMinimum = @PartTimeHoursMaxWeek * HourlyMinimum * @WeeksInYear where PayGrade = 'PT'
--update @salaryMatrix set SalaryMidPoint = SalaryMinimum * @MidpointMultiplier where PayGrade = 'PT'
--update @salaryMatrix set SalaryMaximum = SalaryMidPoint * @MaximumMultiplier where PayGrade = 'PT'
--update @salaryMatrix set HourlyMidPoint = SalaryMidPoint / @PartTimeHoursMaxYear where PayGrade = 'PT'
--update @salaryMatrix set HourlyMaximum = SalaryMidPoint /@PartTimeHoursMaxYear where PayGrade = 'PT'

select 
format(sum(SalaryMinimum),'N') MinimumSalaryTotal,
format(sum(SalaryMidPoint),'N') MaximumSalaryTotal,
format(sum(SalaryMaximum),'N') MaximumSalaryTotal
from @salaryMatrix

select * from @salaryMatrix