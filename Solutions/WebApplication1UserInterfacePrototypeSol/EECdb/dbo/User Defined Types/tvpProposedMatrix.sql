﻿CREATE TYPE [dbo].[tvpProposedMatrix] AS TABLE (
    [PayGrade]           INT             NULL,
    [PercentageIncrease] DECIMAL (18, 4) NULL,
    [PercentageInc]      DECIMAL (5, 2)  NULL,
    [HourlyMinimum]      DECIMAL (18, 4) NULL,
    [HourlyMidPoint]     DECIMAL (18, 4) NULL,
    [HourlyMaximum]      DECIMAL (18, 4) NULL,
    [SalaryMinimum]      DECIMAL (18, 4) NULL,
    [SalaryMidPoint]     DECIMAL (18, 4) NULL,
    [SalaryMaximum]      DECIMAL (18, 4) NULL);

