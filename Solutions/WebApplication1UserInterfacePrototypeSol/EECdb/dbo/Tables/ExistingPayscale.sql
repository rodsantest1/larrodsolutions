﻿CREATE TABLE [dbo].[ExistingPayscale] (
    [Id]               INT            NULL,
    [Position]         NVARCHAR (255) NULL,
    [PositionId]       INT            NULL,
    [MinimumAmount]    MONEY          NULL,
    [MaximumAmount]    MONEY          NULL,
    [ProposedPayGrade] FLOAT (53)     NULL,
    [EffectiveDate]    DATE           NULL
);



