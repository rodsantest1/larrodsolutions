﻿CREATE TABLE [dbo].[EntrySalaryPlusLongevity] (
    [Id]              INT             IDENTITY (1, 1) NOT NULL,
    [YearsOfService]  DECIMAL (18, 2) NULL,
    [RaisePercentage] DECIMAL (18, 2) NULL,
    CONSTRAINT [PK_EntrySalaryPlusLongevity] PRIMARY KEY CLUSTERED ([Id] ASC)
);

