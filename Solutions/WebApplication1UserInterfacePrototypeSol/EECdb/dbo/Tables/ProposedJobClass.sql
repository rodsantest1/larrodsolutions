﻿CREATE TABLE [dbo].[ProposedJobClass] (
    [Position]     NVARCHAR (255) NULL,
    [PositionId]   INT            NULL,
    [JobClassCode] INT            NULL,
    [Grade]        INT            NULL
);

