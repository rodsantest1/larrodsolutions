﻿CREATE TABLE [dbo].[PayGradePercentageIncrease] (
    [Id]                 INT             IDENTITY (1, 1) NOT NULL,
    [PayGrade]           INT             NULL,
    [PercentageIncrease] DECIMAL (18, 2) NULL,
    CONSTRAINT [PK_PayGradePercentageIncrease] PRIMARY KEY CLUSTERED ([Id] ASC)
);

