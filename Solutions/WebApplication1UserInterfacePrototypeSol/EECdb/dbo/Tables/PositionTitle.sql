﻿CREATE TABLE [dbo].[PositionTitle] (
    [Id]           INT            IDENTITY (1, 1) NOT NULL,
    [Name]         NVARCHAR (255) NULL,
    [JobClassCode] INT            NULL,
    [PayGrade]     INT            NULL,
    [src]          INT            NULL,
    CONSTRAINT [PK_PositionTitle] PRIMARY KEY CLUSTERED ([Id] ASC)
);

