﻿CREATE TABLE [dbo].[CustomerImport] (
    [locnam] NVARCHAR (255)  NULL,
    [posnam] NVARCHAR (255)  NULL,
    [posgrd] INT             NULL,
    [poshrs] INT             NULL,
    [minhrs] DECIMAL (18, 4) NULL,
    [maxhrs] DECIMAL (18, 4) NULL,
    [minsal] DECIMAL (18, 4) NULL,
    [maxsal] DECIMAL (18, 4) NULL
);

