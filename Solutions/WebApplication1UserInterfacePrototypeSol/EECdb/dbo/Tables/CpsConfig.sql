﻿CREATE TABLE [dbo].[CPSConfig] (
    [Id]                 INT             IDENTITY (1, 1) NOT NULL,
    [ConfigName]         NVARCHAR (4000) NULL,
    [ConfigValueNumber4] DECIMAL (18, 4) NULL,
    [ConfigValueAplha]   NVARCHAR (4000) NULL,
    CONSTRAINT [PK_Table_1] PRIMARY KEY CLUSTERED ([Id] ASC)
);



