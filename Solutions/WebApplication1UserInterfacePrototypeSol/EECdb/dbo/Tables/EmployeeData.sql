﻿CREATE TABLE [dbo].[EmployeeData] (
    [LastName]         NVARCHAR (255)  NULL,
    [FirstName]        NVARCHAR (255)  NULL,
    [PositionTitle]    NVARCHAR (255)  NULL,
    [PositionId]       INT             NULL,
    [HireDate]         DATE            NULL,
    [Department]       NVARCHAR (255)  NULL,
    [HourlyRate]       DECIMAL (18, 2) NULL,
    [WeeklyHours]      INT             NULL,
    [YearHours]        INT             NULL,
    [CurrentSalary]    AS              ([HourlyRate]*[YearHours]),
    [ProposedJobClass] INT             NULL,
    [YearsOfService]   AS              (datediff(year,[HireDate],getdate())),
    [PositionType]     NVARCHAR (255)  NULL
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Current $/HR ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeData', @level2type = N'COLUMN', @level2name = N'HourlyRate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'[Hours Worked per Week]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeData', @level2type = N'COLUMN', @level2name = N'WeeklyHours';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'[Annual Hours Worked]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeData', @level2type = N'COLUMN', @level2name = N'YearHours';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'[Current Salary]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeData', @level2type = N'COLUMN', @level2name = N'CurrentSalary';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'[Proposed Job Class]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EmployeeData', @level2type = N'COLUMN', @level2name = N'ProposedJobClass';

