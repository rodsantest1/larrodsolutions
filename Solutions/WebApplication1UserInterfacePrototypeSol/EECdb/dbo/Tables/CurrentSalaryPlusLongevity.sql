﻿CREATE TABLE [dbo].[CurrentSalaryPlusLongevity] (
    [Id]              INT             IDENTITY (1, 1) NOT NULL,
    [YearsOfService]  DECIMAL (18, 2) NULL,
    [RaisePercentage] DECIMAL (18, 2) NULL,
    CONSTRAINT [PK_CurrentSalaryPlusLongevity] PRIMARY KEY CLUSTERED ([Id] ASC)
);

