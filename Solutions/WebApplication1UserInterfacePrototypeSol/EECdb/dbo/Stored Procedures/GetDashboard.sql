﻿-- =============================================
-- Author:		Rodney
-- Create date: 9/28/22
-- Description:	Getting all tabs
-- =============================================
CREATE       PROCEDURE [dbo].[GetDashboard]
	-- Add the parameters for the stored procedure here
	@county nvarchar(25) = 'Pierce',
	@startingSalary decimal(18,4) = 15,
	@monthsRemaining int = 8
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--Returns multiple result sets 
	--1. Proposed Matrix 
	--2. Current Vs Proposed 
	--3. Employee Data 
	--4. Quick Glance 
	 
DECLARE @proposedMatrix tvpProposedMatrix

INSERT @proposedMatrix
EXEC [dbo].[GetProposedMatrx] @startingSalary

select 
PayGrade [Pay Grade]
, format((PercentageInc/100),'p0') [% Inc]
, format(HourlyMinimum,'c') [Hourly Minimum]
, format(HourlyMidPoint,'c') [Hourly MidPoint] 
, format(HourlyMaximum,'c') [Hourly Maximum] 
, format(SalaryMinimum,'c') [Salary Minimum]
, format(SalaryMidPoint,'c') [Salary MidPoint] 
, format(SalaryMaximum,'c') [Salary Maximum] 
from @proposedMatrix

EXEC [dbo].[GetCurrentVsProposed] @county, @proposedMatrix

EXEC [dbo].[GetEmployeeData] @proposedMatrix, @monthsRemaining


END