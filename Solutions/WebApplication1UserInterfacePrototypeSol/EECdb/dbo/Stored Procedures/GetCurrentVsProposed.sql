﻿-- =============================================
-- Author:		Rodney
-- Create date: 9/19/22
-- Description:	Current vs Proposed Excel tab
-- =============================================
CREATE       PROCEDURE [dbo].[GetCurrentVsProposed]
	-- Add the parameters for the stored procedure here
	@countyName nvarchar(255),
	@proposedMatrix tvpProposedMatrix readonly
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


declare @selectedLocation nvarchar(25) = @countyName

--min and max averages over all positions
declare @avgPayGrades table (
posnam nvarchar(255),
poshrs int,
cntgrd int,
minavg decimal(18,4),
maxavg decimal(18,4)
)

insert @avgPayGrades
select posnam,poshrs,posgrd, 
AVG(minhrs) minavg, AVG(maxhrs) maxavg
from CustomerImport
where locnam <> @selectedLocation
group by posnam, posgrd, poshrs

declare @proposedPositionGrades table (
locnam nvarchar(255),
posnam nvarchar(255),
ppdgrd int
)

insert @proposedPositionGrades
select locnam, posnam, posgrd from CustomerImport

;with cte as (
select p.locnam, p.posnam, g.poshrs, t.posgrd CurrentPayGrade, x.PayGrade ProposedPayGrade, 
	g.minavg BenchMarkMinimumAverage, 
	g.maxavg BenchmarkMaximumAverage, 
	HourlyMinimum ProposedHourlyMinimumRate, 
	HourlyMidPoint ProposedHourlyMidpointRate, 
	HourlyMaximum ProposedHourlyMaximumRate, 
	(HourlyMinimum * t.poshrs)  ProposedSalaryMinimumRate, 
	(HourlyMidPoint * t.poshrs) ProposedSalaryMidpointRate, 
	(HourlyMaximum * t.poshrs) ProposedSalaryMaximumRate, 
	t.minhrs CurrentMinimumHourlyRate,
	t.maxhrs CurrentMaximumHourlyRate
from @proposedPositionGrades p
join @avgPayGrades g
on p.posnam = g.posnam 
join @proposedMatrix x
on p.ppdgrd = x.PayGrade
join CustomerImport t
on t.locnam = p.locnam
and t.posnam = p.posnam
), cte2 as (
select posnam [Position Name]
	,poshrs [Position Hours]
	,CurrentPayGrade [Current Pay Grade]
	,ProposedPayGrade [Proposed Pay Grade]
	,BenchMarkMinimumAverage [Benchmark Minimum Average]
	,BenchmarkMaximumAverage [Benchmark Maximum Average]
	,CurrentMinimumHourlyRate [Current Minimum Hourly Rate] 
	,ProposedHourlyMinimumRate [Proposed Hourly Minimum Rate]
	,CurrentMaximumHourlyRate [Current Maximum Hourly Rate]
	,(CurrentMinimumHourlyRate * poshrs) [Current Salary Minimum Rate]
	,ProposedSalaryMinimumRate [Proposed Salary Minimum Rate]
	,format(((CurrentMinimumHourlyRate/BenchMarkMinimumAverage)-1),'p') [Pct Diff]
	,(BenchMarkMinimumAverage * poshrs) [Benchmark Minimum Salary]
from cte
where locnam = @selectedLocation
) 
select @selectedLocation 'County' 
,[Position Name]
,[Position Hours]
,[Proposed Pay Grade]
,FORMAT([Benchmark Minimum Average],'C') [Benchmark Minimum Average]
,FORMAT([Benchmark Maximum Average],'C') [Benchmark Maximum Average]
,FORMAT([Current Minimum Hourly Rate],'C') [Current Minimum Hourly Rate]
,FORMAT([Proposed Hourly Minimum Rate],'C') [Proposed Hourly Minimum Rate]
,FORMAT([Current Maximum Hourly Rate],'C') [Current Maximum Hourly Rate]
,FORMAT([Current Salary Minimum Rate],'C') [Current Salary Minimum Rate]
,FORMAT([Proposed Salary Minimum Rate],'C') [Proposed Salary Minimum Rate]
,[Pct Diff]
,FORMAT([Benchmark Minimum Salary],'C') [Benchmark Minimum Salary]
,FORMAT(([Proposed Salary Minimum Rate] - [Benchmark Minimum Salary]),'C') [Increase From Current]
,format((([Proposed Salary Minimum Rate] / [Benchmark Minimum Salary])-1),'p') [% Deviation]
from cte2
order by [Position Name]

END