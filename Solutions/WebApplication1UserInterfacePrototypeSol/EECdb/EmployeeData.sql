declare @remainingMonths int = 8
declare @colaRaise decimal(18,2)
select @colaRaise = (select ConfigValueNumber4 from CPSConfig where ConfigName = 'colaraise')
declare @fica decimal(5,4) = 0.0765
declare @fullTimeRetirementMatch decimal(18,2)
select @fullTimeRetirementMatch = (select ConfigValueNumber4 from CPSConfig where ConfigName = 'fulltimeretirementmatch')
DECLARE @Report_EmployeeData TABLE (
	[RN] [int],
	[LastName] [nvarchar](255),
	[FirstName] [nvarchar](255),
	[HireDate] [date],
	[PositionName] [nvarchar](255),
	[PositionId] [int],
	[PositionType] [nvarchar](255),
	[Department] [nvarchar](255),
	[HourlyRate] [decimal](18, 2),
	[WeeklyHours] [int],
	[YearHours] [int],
	[CurrentSalary] [decimal](29, 2),
	[ProposedJobClass] [int],
	[YearsOfService] [int],
	[HourlyMinimum] [decimal](18, 4),
	[EntrySalaryAtNewGradeAndMatrix] [decimal](29, 4),
	[EntrySalaryPlusLongevity] [decimal](38, 4),
	[CurrentSalaryPlusLongevity] [decimal](38, 4),
	[SalaryWithCola] [decimal](38, 4),
	[MaxProposedSalary] [decimal](38, 4),
	[SalaryIncrease] [decimal](38, 4),
	[PctIncrease] [decimal](38, 4),
	[FicaIncrease] [decimal](38, 4),
	[RetirementIncrease] [decimal](38, 4),
	[TotalBudgetImpact] [decimal](38, 4) NULL
)

insert @Report_EmployeeData (
LastName
,FirstName
,HireDate
,PositionName
,PositionId
,PositionType
,Department
,HourlyRate
,WeeklyHours
,YearHours
,CurrentSalary
,ProposedJobClass
,YearsOfService
,HourlyMinimum)
select
e.LastName
,e.FirstName
,e.HireDate
,p.[Name]
,p.Id PositionId
,e.PositionType
,e.Department
,e.HourlyRate
,e.WeeklyHours
,e.YearHours
,e.CurrentSalary
,e.ProposedJobClass
,e.YearsOfService
,x.HourlyMinimum
from EmployeeData e
left join PositionTitle p
on e.PositionId = p.Id
left join ProposedMatrix x
on p.PayGrade = x.PayGrade
where CurrentSalary > 0

update @Report_EmployeeData set EntrySalaryAtNewGradeAndMatrix = (HourlyMinimum * YearHours)
update @Report_EmployeeData set 
	EntrySalaryPlusLongevity = ((1 + l.RaisePercentage) * EntrySalaryAtNewGradeAndMatrix),
	CurrentSalaryPlusLongevity = ((1 + c.RaisePercentage) * CurrentSalary),
	SalaryWithCola = (CurrentSalary * (1+(@colaRaise)))
	from @Report_EmployeeData e
	join EntrySalaryPlusLongevity l
	on e.YearsOfService = l.YearsOfService
	join CurrentSalaryPlusLongevity c
	on c.YearsOfService = e.YearsOfService
update @Report_EmployeeData set MaxProposedSalary = (select max(v) from (values (EntrySalaryPlusLongevity), (CurrentSalaryPlusLongevity), (SalaryWithCola)) as value(v))
update @Report_EmployeeData set SalaryIncrease = (MaxProposedSalary - CurrentSalary)
update @Report_EmployeeData set PctIncrease = ((MaxProposedSalary / CurrentSalary) -1) * 100
update @Report_EmployeeData set FicaIncrease = (SalaryIncrease * @fica)
update @Report_EmployeeData set RetirementIncrease = (SalaryIncrease * (@fullTimeRetirementMatch))
update @Report_EmployeeData set TotalBudgetImpact = (SalaryIncrease + FicaIncrease + RetirementIncrease)

;with cte as (
select ROW_NUMBER() over (partition by positiontype order by positionname) RN2, * 
from @Report_EmployeeData
)
update cte set RN=RN2

SELECT * FROM @Report_EmployeeData

DECLARE @Report_Totals TABLE (
	[Note] [varchar](255),
	[TotalType] [nvarchar](255),
	[SalaryIncrease] [decimal](38, 4),
	[FicaIncrease] [decimal](38, 4),
	[RetirementIncrease] [decimal](38, 4),
	[TotalBudgetImpact] [decimal](38, 4),
	[SalaryIncrease_Rpt] [nvarchar](4000),
	[FicaIncrease_Rpt] [nvarchar](4000),
	[RetirementIncrease_Rpt] [nvarchar](4000),
	[TotalBudgetImpact_Rpt] [nvarchar](4000) NULL
) 

;with cte as (
SELECT 'I. Full Time Totals' 'Note', 'FT' TotalType,
SUM(SalaryIncrease) SalaryIncrease
,SUM(FicaIncrease) FicaIncrease
,SUM(RetirementIncrease) RetirementIncrease
,SUM(TotalBudgetImpact) TotalBudgetImpact
,format(SUM(salaryincrease),'c') SalaryIncrease_Rpt
,format(SUM(FicaIncrease),'c') FicaIncrease_Rpt
,format(SUM(RetirementIncrease),'c') RetirementIncrease_Rpt
,format(SUM(TotalBudgetImpact),'c') TotalBudgetImpact_Rpt
FROM @Report_EmployeeData WHERE PositionType = 'FT'
UNION ALL
SELECT 'II. Part Time Totals' 'Note','PT' TotalType,
SUM(SalaryIncrease) SalaryIncrease
,SUM(FicaIncrease) FicaIncrease
,SUM(RetirementIncrease) RetirementIncrease
,SUM(TotalBudgetImpact) TotalBudgetImpact
,format(SUM(salaryincrease),'c') SalaryIncrease_Rpt
,format(SUM(FicaIncrease),'c') FicaIncrease_Rpt
,format(SUM(RetirementIncrease),'c') RetirementIncrease_Rpt
,format(SUM(TotalBudgetImpact),'c') TotalBudgetImpact_Rpt
FROM @Report_EmployeeData WHERE PositionType = 'PT'
UNION ALL
SELECT 'III. Special Position Totals' 'Note','SP' TotalType,
SUM(SalaryIncrease) SalaryIncrease
,SUM(FicaIncrease) FicaIncrease
,SUM(RetirementIncrease) RetirementIncrease
,SUM(TotalBudgetImpact) TotalBudgetImpact
,format(SUM(salaryincrease),'c') SalaryIncrease_Rpt
,format(SUM(FicaIncrease),'c') FicaIncrease_Rpt
,format(SUM(RetirementIncrease),'c') RetirementIncrease_Rpt
,format(SUM(TotalBudgetImpact),'c') TotalBudgetImpact_Rpt
FROM @Report_EmployeeData WHERE PositionType = 'SP'
)
insert @Report_Totals
SELECT * FROM cte

INSERT @Report_Totals
SELECT 'IV. Grand Totals' 'Note','GT'
,SUM(SalaryIncrease) SalaryIncrease
,SUM(FicaIncrease) FicaIncrease
,SUM(RetirementIncrease) RetirementIncrease
,SUM(TotalBudgetImpact) TotalBudgetImpact
,FORMAT(SUM(SalaryIncrease),'C') SalaryIncrease_Rpt
,FORMAT(SUM(FicaIncrease),'C') FicaIncrease_Rpt
,FORMAT(SUM(RetirementIncrease),'C') RetirementIncrease_Rpt
,FORMAT(SUM(TotalBudgetImpact),'C') TotalBudgetImpact_Rpt
FROM @Report_Totals

--unpivot the table for Quick Glance tab
DECLARE @quickGlance table (
[Note] [nvarchar](252),
[Increases] [nvarchar](252),
[Amounts] [decimal](18,4)
)

INSERT @quickGlance
SELECT Note, Increases, Amounts
FROM   
   (SELECT Note, SalaryIncrease, FicaIncrease, RetirementIncrease, TotalBudgetImpact
   FROM @Report_Totals) p  
UNPIVOT  
   (Amounts FOR Increases IN   
      (SalaryIncrease, FicaIncrease, RetirementIncrease, TotalBudgetImpact)  
)AS unpvt;  

INSERT @quickGlance
SELECT 'V. Cost to Implement', 'RM',
(SELECT TotalBudgetImpact FROM @Report_Totals WHERE Note = 'IV. Grand Totals')/12*@remainingMonths

SELECT 
Note = case when row_number() over (partition by Note order by (Select null)) = 1 then Note end 
, Increases
, format(amounts,'c') Amounts 
FROM @quickGlance
