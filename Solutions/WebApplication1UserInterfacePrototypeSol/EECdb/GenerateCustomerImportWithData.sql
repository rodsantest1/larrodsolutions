--USE [EECdb]
GO
/****** Object:  Table [dbo].[CustomerImport]    Script Date: 9/19/22 7:08:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CustomerImport](
	[locnam] [nvarchar](255) NULL,
	[posnam] [nvarchar](255) NULL,
	[posgrd] [int] NULL,
	[poshrs] [int] NULL,
	[minhrs] [decimal](18, 4) NULL,
	[maxhrs] [decimal](18, 4) NULL,
	[minsal] [decimal](18, 4) NULL,
	[maxsal] [decimal](18, 4) NULL
) ON [PRIMARY]
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pierce', N'ACCOUNTING CLERK ', 9, 2080, CAST(12.7300 AS Decimal(18, 4)), CAST(13.5300 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pierce', N'ADMIN ASSISTANT ', 5, 2080, CAST(11.4100 AS Decimal(18, 4)), CAST(14.2800 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pierce', N'AP CLERK', 9, 2080, CAST(13.6600 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pierce', N'APPRAISER ', 6, 2080, CAST(13.8000 AS Decimal(18, 4)), CAST(17.5100 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pierce', N'CAPTAIN ADMIN ', 13, 2496, CAST(18.0400 AS Decimal(18, 4)), CAST(23.6600 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pierce', N'CHIEF APPRAISER', 23, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pierce', N'CHIEF DEPUTY CLERK ', 9, 2080, CAST(13.5400 AS Decimal(18, 4)), CAST(15.5000 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pierce', N'CHIEF DEPUTY SHERIFF ', 26, 2080, CAST(20.1600 AS Decimal(18, 4)), CAST(21.2300 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pierce', N'CHIEF DEPUTY TAX COMMISSIONER', 9, 2080, CAST(14.2800 AS Decimal(18, 4)), CAST(16.3200 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pierce', N'CLERK ', 5, 2080, CAST(11.4100 AS Decimal(18, 4)), CAST(13.1500 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pierce', N'COMMUNICATIONS OFFICER ', 6, 2496, CAST(11.7500 AS Decimal(18, 4)), CAST(13.9800 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pierce', N'COURTS - CHIEF CLERK/PT MAGISTRATE', 17, 2080, CAST(18.7900 AS Decimal(18, 4)), CAST(20.2600 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pierce', N'CUSTODIAN', 5, 2080, CAST(12.2400 AS Decimal(18, 4)), CAST(15.0000 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pierce', N'DEPUTY ', 9, 2496, CAST(14.0700 AS Decimal(18, 4)), CAST(14.8200 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pierce', N'DEPUTY CLERK', 6, 2496, CAST(11.4100 AS Decimal(18, 4)), CAST(14.0400 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pierce', N'DEPUTY FIRE CHIEF', 10, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pierce', N'DEPUTY, LIEUTENANT', 12, 2496, CAST(16.7200 AS Decimal(18, 4)), CAST(17.7800 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pierce', N'DEPUTY, SGT', 11, 2496, CAST(15.2600 AS Decimal(18, 4)), CAST(16.0300 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pierce', N'DETENTION OFFICER  ', 6, 2496, CAST(11.2200 AS Decimal(18, 4)), CAST(14.5600 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pierce', N'DETENTION OFFICER SGT', 8, 2496, CAST(12.5500 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pierce', N'DETENTION OFFICER, CORP', 7, 2496, CAST(13.5900 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pierce', N'DETENTION OFFICER, LT', 10, 2496, CAST(13.9700 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pierce', N'ELECTIONS SUPERVISOR ', 9, 2080, CAST(16.9800 AS Decimal(18, 4)), CAST(18.4000 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pierce', N'EMERGENCY MEDCIAL TECHNICIAN (EMT)', 6, 2496, CAST(13.7500 AS Decimal(18, 4)), CAST(17.2500 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pierce', N'EMS DIRECTOR', 26, 2080, CAST(27.8800 AS Decimal(18, 4)), CAST(31.2500 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pierce', N'FACILITIES DIRECTOR', 26, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pierce', N'FACILITIES MAINTENANCE TECHNICIAN', 16, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pierce', N'FIRE/EMA CHIEF', 27, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pierce', N'GROUNDSKEEPER', 5, 2080, CAST(9.5500 AS Decimal(18, 4)), CAST(0.0000 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pierce', N'HUMAN RESOURCES GENERALIST', 10, 2080, CAST(16.0000 AS Decimal(18, 4)), CAST(23.0000 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pierce', N'INVESTIGATOR ', 10, 2496, CAST(16.3400 AS Decimal(18, 4)), CAST(17.4600 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pierce', N'INVESTIGATOR, SGT', 11, 2496, CAST(16.7200 AS Decimal(18, 4)), CAST(18.2400 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pierce', N'JAIL ADMINISTRATION', 12, 2496, CAST(16.7200 AS Decimal(18, 4)), CAST(17.4300 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pierce', N'LABORER  ', 5, 2080, CAST(12.2400 AS Decimal(18, 4)), CAST(15.0000 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pierce', N'LEAD PARAMEDIC', 14, 2496, CAST(18.7500 AS Decimal(18, 4)), CAST(22.2500 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pierce', N'OPERATOR ', 9, 2080, CAST(15.3000 AS Decimal(18, 4)), CAST(17.0000 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pierce', N'PARAMEDIC', 12, 2080, CAST(17.7500 AS Decimal(18, 4)), CAST(21.2500 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pierce', N'PARKS & RECREATION DIRECTOR', 23, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pierce', N'REC COORDINATOR', 6, 2080, CAST(11.2200 AS Decimal(18, 4)), CAST(13.2600 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pierce', N'REC FACILITIES ASSISTANT ', 5, 2080, CAST(10.6100 AS Decimal(18, 4)), CAST(12.0000 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pierce', N'REC FACILITIES TEAM LEADER ', 7, 2080, CAST(12.7300 AS Decimal(18, 4)), CAST(15.0000 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pierce', N'REGISTRAR', 6, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pierce', N'ROAD CREW LEADER', 11, 2080, CAST(16.3200 AS Decimal(18, 4)), CAST(19.3200 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pierce', N'ROAD SUPERINTENDENT', 27, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pierce', N'ZONING ADMINISTRATOR ', 9, 2080, CAST(14.8600 AS Decimal(18, 4)), CAST(15.7400 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Brantley', N'ACCOUNTING CLERK ', 9, 2080, CAST(17.2000 AS Decimal(18, 4)), CAST(23.6100 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Brantley', N'ADMIN ASSISTANT ', 5, 2080, CAST(12.6100 AS Decimal(18, 4)), CAST(17.1500 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Brantley', N'AP CLERK', 9, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Brantley', N'APPRAISER ', 6, 2080, CAST(13.7700 AS Decimal(18, 4)), CAST(18.7800 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Brantley', N'CAPTAIN ADMIN ', 13, 2496, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Brantley', N'CHIEF APPRAISER', 23, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Brantley', N'CHIEF DEPUTY CLERK ', 9, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Brantley', N'CHIEF DEPUTY SHERIFF ', 26, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Brantley', N'CHIEF DEPUTY TAX COMMISSIONER', 9, 2080, CAST(17.2000 AS Decimal(18, 4)), CAST(23.6100 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Brantley', N'CLERK ', 5, 2080, CAST(11.0800 AS Decimal(18, 4)), CAST(15.0000 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Brantley', N'COMMUNICATIONS OFFICER ', 6, 2496, CAST(11.5700 AS Decimal(18, 4)), CAST(15.6800 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Brantley', N'COURTS - CHIEF CLERK/PT MAGISTRATE', 17, 2080, CAST(13.7700 AS Decimal(18, 4)), CAST(18.7800 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Brantley', N'CUSTODIAN', 5, 2080, CAST(10.1800 AS Decimal(18, 4)), CAST(13.7300 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Brantley', N'DEPUTY ', 9, 2496, CAST(13.7700 AS Decimal(18, 4)), CAST(18.7800 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Brantley', N'DEPUTY CLERK', 6, 2496, CAST(11.5700 AS Decimal(18, 4)), CAST(15.6800 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Brantley', N'DEPUTY FIRE CHIEF', 10, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Brantley', N'DEPUTY, LIEUTENANT', 12, 2496, CAST(17.2000 AS Decimal(18, 4)), CAST(23.6100 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Brantley', N'DEPUTY, SGT', 11, 2496, CAST(15.7200 AS Decimal(18, 4)), CAST(21.5300 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Brantley', N'DETENTION OFFICER  ', 6, 2496, CAST(12.6100 AS Decimal(18, 4)), CAST(17.1500 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Brantley', N'DETENTION OFFICER SGT', 8, 2496, CAST(15.0400 AS Decimal(18, 4)), CAST(20.5700 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Brantley', N'DETENTION OFFICER, CORP', 7, 2496, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Brantley', N'DETENTION OFFICER, LT', 10, 2496, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Brantley', N'ELECTIONS SUPERVISOR ', 9, 2080, CAST(15.0400 AS Decimal(18, 4)), CAST(20.5700 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Brantley', N'EMERGENCY MEDCIAL TECHNICIAN (EMT)', 6, 2496, CAST(12.6100 AS Decimal(18, 4)), CAST(17.1500 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Brantley', N'EMS DIRECTOR', 26, 2080, CAST(23.6900 AS Decimal(18, 4)), CAST(32.7500 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Brantley', N'FACILITIES DIRECTOR', 26, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Brantley', N'FACILITIES MAINTENANCE TECHNICIAN', 16, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Brantley', N'FIRE/EMA CHIEF', 27, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Brantley', N'GROUNDSKEEPER', 5, 2080, CAST(11.0800 AS Decimal(18, 4)), CAST(15.0000 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Brantley', N'HUMAN RESOURCES GENERALIST', 10, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Brantley', N'INVESTIGATOR ', 10, 2496, CAST(15.0400 AS Decimal(18, 4)), CAST(20.5700 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Brantley', N'INVESTIGATOR, SGT', 11, 2496, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Brantley', N'JAIL ADMINISTRATION', 12, 2496, CAST(19.7100 AS Decimal(18, 4)), CAST(27.1500 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Brantley', N'LABORER  ', 5, 2080, CAST(11.0800 AS Decimal(18, 4)), CAST(15.0000 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Brantley', N'LEAD PARAMEDIC', 14, 2496, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Brantley', N'OPERATOR ', 9, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Brantley', N'PARAMEDIC', 12, 2080, CAST(15.7200 AS Decimal(18, 4)), CAST(21.5300 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Brantley', N'PARKS & RECREATION DIRECTOR', 23, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Brantley', N'REC COORDINATOR', 6, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Brantley', N'REC FACILITIES ASSISTANT ', 5, 2080, CAST(11.0800 AS Decimal(18, 4)), CAST(15.0000 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Brantley', N'REC FACILITIES TEAM LEADER ', 7, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Brantley', N'REGISTRAR', 6, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Brantley', N'ROAD CREW LEADER', 11, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Brantley', N'ROAD SUPERINTENDENT', 27, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Brantley', N'ZONING ADMINISTRATOR ', 9, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Elbert', N'ACCOUNTING CLERK ', 9, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Elbert', N'ADMIN ASSISTANT ', 5, 2080, CAST(13.4500 AS Decimal(18, 4)), CAST(18.5500 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Elbert', N'AP CLERK', 9, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Elbert', N'APPRAISER ', 6, 2080, CAST(10.0000 AS Decimal(18, 4)), CAST(19.2500 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Elbert', N'CAPTAIN ADMIN ', 13, 2496, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Elbert', N'CHIEF APPRAISER', 23, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Elbert', N'CHIEF DEPUTY CLERK ', 9, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Elbert', N'CHIEF DEPUTY SHERIFF ', 26, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Elbert', N'CHIEF DEPUTY TAX COMMISSIONER', 9, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Elbert', N'CLERK ', 5, 2080, CAST(13.0000 AS Decimal(18, 4)), CAST(13.0000 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Elbert', N'COMMUNICATIONS OFFICER ', 6, 2496, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Elbert', N'COURTS - CHIEF CLERK/PT MAGISTRATE', 17, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Elbert', N'CUSTODIAN', 5, 2080, CAST(13.0000 AS Decimal(18, 4)), CAST(13.0000 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Elbert', N'DEPUTY ', 9, 2496, CAST(12.2500 AS Decimal(18, 4)), CAST(18.4200 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Elbert', N'DEPUTY CLERK', 6, 2496, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Elbert', N'DEPUTY FIRE CHIEF', 10, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Elbert', N'DEPUTY, LIEUTENANT', 12, 2496, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Elbert', N'DEPUTY, SGT', 11, 2496, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Elbert', N'DETENTION OFFICER  ', 6, 2496, CAST(12.2500 AS Decimal(18, 4)), CAST(19.6700 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Elbert', N'DETENTION OFFICER SGT', 8, 2496, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Elbert', N'DETENTION OFFICER, CORP', 7, 2496, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Elbert', N'DETENTION OFFICER, LT', 10, 2496, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Elbert', N'ELECTIONS SUPERVISOR ', 9, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Elbert', N'EMERGENCY MEDCIAL TECHNICIAN (EMT)', 6, 2496, CAST(12.4000 AS Decimal(18, 4)), CAST(20.7500 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Elbert', N'EMS DIRECTOR', 26, 2080, CAST(21.7500 AS Decimal(18, 4)), CAST(21.7500 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Elbert', N'FACILITIES DIRECTOR', 26, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Elbert', N'FACILITIES MAINTENANCE TECHNICIAN', 16, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Elbert', N'FIRE/EMA CHIEF', 27, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Elbert', N'GROUNDSKEEPER', 5, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Elbert', N'HUMAN RESOURCES GENERALIST', 10, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Elbert', N'INVESTIGATOR ', 10, 2496, CAST(18.4200 AS Decimal(18, 4)), CAST(20.1700 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Elbert', N'INVESTIGATOR, SGT', 11, 2496, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Elbert', N'JAIL ADMINISTRATION', 12, 2496, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Elbert', N'LABORER  ', 5, 2080, CAST(10.5000 AS Decimal(18, 4)), CAST(12.5000 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Elbert', N'LEAD PARAMEDIC', 14, 2496, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Elbert', N'OPERATOR ', 9, 2080, CAST(12.0000 AS Decimal(18, 4)), CAST(22.0000 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Elbert', N'PARAMEDIC', 12, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Elbert', N'PARKS & RECREATION DIRECTOR', 23, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Elbert', N'REC COORDINATOR', 6, 2080, CAST(10.0000 AS Decimal(18, 4)), CAST(14.2500 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Elbert', N'REC FACILITIES ASSISTANT ', 5, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Elbert', N'REC FACILITIES TEAM LEADER ', 7, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Elbert', N'REGISTRAR', 6, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Elbert', N'ROAD CREW LEADER', 11, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Elbert', N'ROAD SUPERINTENDENT', 27, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Elbert', N'ZONING ADMINISTRATOR ', 9, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Appling', N'ACCOUNTING CLERK ', 9, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Appling', N'ADMIN ASSISTANT ', 5, 2080, CAST(14.5500 AS Decimal(18, 4)), CAST(20.2100 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Appling', N'AP CLERK', 9, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Appling', N'APPRAISER ', 6, 2080, CAST(10.0000 AS Decimal(18, 4)), CAST(15.5600 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Appling', N'CAPTAIN ADMIN ', 13, 2496, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Appling', N'CHIEF APPRAISER', 23, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Appling', N'CHIEF DEPUTY CLERK ', 9, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Appling', N'CHIEF DEPUTY SHERIFF ', 26, 2080, CAST(21.6300 AS Decimal(18, 4)), CAST(30.1900 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Appling', N'CHIEF DEPUTY TAX COMMISSIONER', 9, 2080, CAST(12.3700 AS Decimal(18, 4)), CAST(15.5400 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Appling', N'CLERK ', 5, 2080, CAST(10.8400 AS Decimal(18, 4)), CAST(14.9400 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Appling', N'COMMUNICATIONS OFFICER ', 6, 2496, CAST(9.0000 AS Decimal(18, 4)), CAST(13.9800 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Appling', N'COURTS - CHIEF CLERK/PT MAGISTRATE', 17, 2080, CAST(10.8400 AS Decimal(18, 4)), CAST(14.9400 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Appling', N'CUSTODIAN', 5, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Appling', N'DEPUTY ', 9, 2496, CAST(13.5000 AS Decimal(18, 4)), CAST(22.2800 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Appling', N'DEPUTY CLERK', 6, 2496, CAST(13.4800 AS Decimal(18, 4)), CAST(20.2800 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Appling', N'DEPUTY FIRE CHIEF', 10, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Appling', N'DEPUTY, LIEUTENANT', 12, 2496, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Appling', N'DEPUTY, SGT', 11, 2496, CAST(17.9100 AS Decimal(18, 4)), CAST(22.2800 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Appling', N'DETENTION OFFICER  ', 6, 2496, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Appling', N'DETENTION OFFICER SGT', 8, 2496, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Appling', N'DETENTION OFFICER, CORP', 7, 2496, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Appling', N'DETENTION OFFICER, LT', 10, 2496, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Appling', N'ELECTIONS SUPERVISOR ', 9, 2080, CAST(13.7300 AS Decimal(18, 4)), CAST(18.0400 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Appling', N'EMERGENCY MEDCIAL TECHNICIAN (EMT)', 6, 2496, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Appling', N'EMS DIRECTOR', 26, 2080, CAST(25.0000 AS Decimal(18, 4)), CAST(28.5400 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Appling', N'FACILITIES DIRECTOR', 26, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Appling', N'FACILITIES MAINTENANCE TECHNICIAN', 16, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Appling', N'FIRE/EMA CHIEF', 27, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Appling', N'GROUNDSKEEPER', 5, 2080, CAST(11.0000 AS Decimal(18, 4)), CAST(12.7300 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Appling', N'HUMAN RESOURCES GENERALIST', 10, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Appling', N'INVESTIGATOR ', 10, 2496, CAST(16.2900 AS Decimal(18, 4)), CAST(23.3500 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Appling', N'INVESTIGATOR, SGT', 11, 2496, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Appling', N'JAIL ADMINISTRATION', 12, 2496, CAST(24.0400 AS Decimal(18, 4)), CAST(26.8200 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Appling', N'LABORER  ', 5, 2080, CAST(11.0000 AS Decimal(18, 4)), CAST(12.7300 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Appling', N'LEAD PARAMEDIC', 14, 2496, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Appling', N'OPERATOR ', 9, 2080, CAST(12.0000 AS Decimal(18, 4)), CAST(18.6700 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Appling', N'PARAMEDIC', 12, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Appling', N'PARKS & RECREATION DIRECTOR', 23, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Appling', N'REC COORDINATOR', 6, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Appling', N'REC FACILITIES ASSISTANT ', 5, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Appling', N'REC FACILITIES TEAM LEADER ', 7, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Appling', N'REGISTRAR', 6, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Appling', N'ROAD CREW LEADER', 11, 2080, CAST(18.0000 AS Decimal(18, 4)), CAST(26.5200 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Appling', N'ROAD SUPERINTENDENT', 27, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Appling', N'ZONING ADMINISTRATOR ', 9, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Berrien', N'ACCOUNTING CLERK ', 9, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Berrien', N'ADMIN ASSISTANT ', 5, 2080, CAST(10.0000 AS Decimal(18, 4)), CAST(16.0000 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Berrien', N'AP CLERK', 9, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Berrien', N'APPRAISER ', 6, 2080, CAST(10.0000 AS Decimal(18, 4)), CAST(16.0000 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Berrien', N'CAPTAIN ADMIN ', 13, 2496, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Berrien', N'CHIEF APPRAISER', 23, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Berrien', N'CHIEF DEPUTY CLERK ', 9, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Berrien', N'CHIEF DEPUTY SHERIFF ', 26, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Berrien', N'CHIEF DEPUTY TAX COMMISSIONER', 9, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Berrien', N'CLERK ', 5, 2080, CAST(10.0000 AS Decimal(18, 4)), CAST(13.0000 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Berrien', N'COMMUNICATIONS OFFICER ', 6, 2496, CAST(12.5000 AS Decimal(18, 4)), CAST(13.2500 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Berrien', N'COURTS - CHIEF CLERK/PT MAGISTRATE', 17, 2080, CAST(10.0000 AS Decimal(18, 4)), CAST(15.0000 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Berrien', N'CUSTODIAN', 5, 2080, CAST(9.0000 AS Decimal(18, 4)), CAST(14.0000 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Berrien', N'DEPUTY ', 9, 2496, CAST(15.0000 AS Decimal(18, 4)), CAST(20.0000 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Berrien', N'DEPUTY CLERK', 6, 2496, CAST(10.0000 AS Decimal(18, 4)), CAST(14.0000 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Berrien', N'DEPUTY FIRE CHIEF', 10, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Berrien', N'DEPUTY, LIEUTENANT', 12, 2496, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Berrien', N'DEPUTY, SGT', 11, 2496, CAST(15.0000 AS Decimal(18, 4)), CAST(18.0000 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Berrien', N'DETENTION OFFICER  ', 6, 2496, CAST(12.7500 AS Decimal(18, 4)), CAST(13.2500 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Berrien', N'DETENTION OFFICER SGT', 8, 2496, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Berrien', N'DETENTION OFFICER, CORP', 7, 2496, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Berrien', N'DETENTION OFFICER, LT', 10, 2496, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Berrien', N'ELECTIONS SUPERVISOR ', 9, 2080, CAST(14.4200 AS Decimal(18, 4)), CAST(17.7900 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Berrien', N'EMERGENCY MEDCIAL TECHNICIAN (EMT)', 6, 2496, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Berrien', N'EMS DIRECTOR', 26, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Berrien', N'FACILITIES DIRECTOR', 26, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Berrien', N'FACILITIES MAINTENANCE TECHNICIAN', 16, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Berrien', N'FIRE/EMA CHIEF', 27, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Berrien', N'GROUNDSKEEPER', 5, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Berrien', N'HUMAN RESOURCES GENERALIST', 10, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Berrien', N'INVESTIGATOR ', 10, 2496, CAST(15.0000 AS Decimal(18, 4)), CAST(21.0000 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Berrien', N'INVESTIGATOR, SGT', 11, 2496, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Berrien', N'JAIL ADMINISTRATION', 12, 2496, CAST(15.0000 AS Decimal(18, 4)), CAST(19.0000 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Berrien', N'LABORER  ', 5, 2080, CAST(10.0000 AS Decimal(18, 4)), CAST(13.0000 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Berrien', N'LEAD PARAMEDIC', 14, 2496, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Berrien', N'OPERATOR ', 9, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Berrien', N'PARAMEDIC', 12, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Berrien', N'PARKS & RECREATION DIRECTOR', 23, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Berrien', N'REC COORDINATOR', 6, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Berrien', N'REC FACILITIES ASSISTANT ', 5, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Berrien', N'REC FACILITIES TEAM LEADER ', 7, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Berrien', N'REGISTRAR', 6, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Berrien', N'ROAD CREW LEADER', 11, 2080, CAST(12.0100 AS Decimal(18, 4)), CAST(21.6500 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Berrien', N'ROAD SUPERINTENDENT', 27, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Berrien', N'ZONING ADMINISTRATOR ', 9, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Dodge', N'ACCOUNTING CLERK ', 9, 2080, CAST(16.8500 AS Decimal(18, 4)), CAST(16.8500 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Dodge', N'ADMIN ASSISTANT ', 5, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Dodge', N'AP CLERK', 9, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Dodge', N'APPRAISER ', 6, 2080, CAST(11.6000 AS Decimal(18, 4)), CAST(12.8500 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Dodge', N'CAPTAIN ADMIN ', 13, 2496, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Dodge', N'CHIEF APPRAISER', 23, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Dodge', N'CHIEF DEPUTY CLERK ', 9, 2080, CAST(14.5800 AS Decimal(18, 4)), CAST(16.1900 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Dodge', N'CHIEF DEPUTY SHERIFF ', 26, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Dodge', N'CHIEF DEPUTY TAX COMMISSIONER', 9, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Dodge', N'CLERK ', 5, 2080, CAST(12.3600 AS Decimal(18, 4)), CAST(14.1400 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Dodge', N'COMMUNICATIONS OFFICER ', 6, 2496, CAST(9.5000 AS Decimal(18, 4)), CAST(13.0000 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Dodge', N'COURTS - CHIEF CLERK/PT MAGISTRATE', 17, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Dodge', N'CUSTODIAN', 5, 2080, CAST(12.0000 AS Decimal(18, 4)), CAST(12.0000 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Dodge', N'DEPUTY ', 9, 2496, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Dodge', N'DEPUTY CLERK', 6, 2496, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Dodge', N'DEPUTY FIRE CHIEF', 10, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Dodge', N'DEPUTY, LIEUTENANT', 12, 2496, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Dodge', N'DEPUTY, SGT', 11, 2496, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Dodge', N'DETENTION OFFICER  ', 6, 2496, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Dodge', N'DETENTION OFFICER SGT', 8, 2496, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Dodge', N'DETENTION OFFICER, CORP', 7, 2496, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Dodge', N'DETENTION OFFICER, LT', 10, 2496, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Dodge', N'ELECTIONS SUPERVISOR ', 9, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Dodge', N'EMERGENCY MEDCIAL TECHNICIAN (EMT)', 6, 2496, CAST(10.7500 AS Decimal(18, 4)), CAST(17.8600 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Dodge', N'EMS DIRECTOR', 26, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Dodge', N'FACILITIES DIRECTOR', 26, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Dodge', N'FACILITIES MAINTENANCE TECHNICIAN', 16, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Dodge', N'FIRE/EMA CHIEF', 27, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Dodge', N'GROUNDSKEEPER', 5, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Dodge', N'HUMAN RESOURCES GENERALIST', 10, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Dodge', N'INVESTIGATOR ', 10, 2496, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Dodge', N'INVESTIGATOR, SGT', 11, 2496, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Dodge', N'JAIL ADMINISTRATION', 12, 2496, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Dodge', N'LABORER  ', 5, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Dodge', N'LEAD PARAMEDIC', 14, 2496, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Dodge', N'OPERATOR ', 9, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Dodge', N'PARAMEDIC', 12, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Dodge', N'PARKS & RECREATION DIRECTOR', 23, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Dodge', N'REC COORDINATOR', 6, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Dodge', N'REC FACILITIES ASSISTANT ', 5, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Dodge', N'REC FACILITIES TEAM LEADER ', 7, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Dodge', N'REGISTRAR', 6, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Dodge', N'ROAD CREW LEADER', 11, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Dodge', N'ROAD SUPERINTENDENT', 27, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Dodge', N'ZONING ADMINISTRATOR ', 9, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Lamar', N'ACCOUNTING CLERK ', 9, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Lamar', N'ADMIN ASSISTANT ', 5, 2080, CAST(18.1700 AS Decimal(18, 4)), CAST(27.6500 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Lamar', N'AP CLERK', 9, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Lamar', N'APPRAISER ', 6, 2080, CAST(12.4500 AS Decimal(18, 4)), CAST(21.7300 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Lamar', N'CAPTAIN ADMIN ', 13, 2496, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Lamar', N'CHIEF APPRAISER', 23, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Lamar', N'CHIEF DEPUTY CLERK ', 9, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Lamar', N'CHIEF DEPUTY SHERIFF ', 26, 2080, CAST(23.1200 AS Decimal(18, 4)), CAST(35.1800 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Lamar', N'CHIEF DEPUTY TAX COMMISSIONER', 9, 2080, CAST(16.3900 AS Decimal(18, 4)), CAST(24.9400 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Lamar', N'CLERK ', 5, 2080, CAST(13.3300 AS Decimal(18, 4)), CAST(20.2900 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Lamar', N'COMMUNICATIONS OFFICER ', 6, 2496, CAST(11.6200 AS Decimal(18, 4)), CAST(17.8600 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Lamar', N'COURTS - CHIEF CLERK/PT MAGISTRATE', 17, 2080, CAST(13.3300 AS Decimal(18, 4)), CAST(20.2900 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Lamar', N'CUSTODIAN', 5, 2080, CAST(11.6200 AS Decimal(18, 4)), CAST(17.6800 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Lamar', N'DEPUTY ', 9, 2496, CAST(15.3000 AS Decimal(18, 4)), CAST(23.2800 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Lamar', N'DEPUTY CLERK', 6, 2496, CAST(13.3300 AS Decimal(18, 4)), CAST(20.2900 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Lamar', N'DEPUTY FIRE CHIEF', 10, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Lamar', N'DEPUTY, LIEUTENANT', 12, 2496, CAST(20.8500 AS Decimal(18, 4)), CAST(31.7300 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Lamar', N'DEPUTY, SGT', 11, 2496, CAST(15.3000 AS Decimal(18, 4)), CAST(23.2800 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Lamar', N'DETENTION OFFICER  ', 6, 2496, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Lamar', N'DETENTION OFFICER SGT', 8, 2496, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Lamar', N'DETENTION OFFICER, CORP', 7, 2496, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Lamar', N'DETENTION OFFICER, LT', 10, 2496, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Lamar', N'ELECTIONS SUPERVISOR ', 9, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Lamar', N'EMERGENCY MEDCIAL TECHNICIAN (EMT)', 6, 2496, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Lamar', N'EMS DIRECTOR', 26, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Lamar', N'FACILITIES DIRECTOR', 26, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Lamar', N'FACILITIES MAINTENANCE TECHNICIAN', 16, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Lamar', N'FIRE/EMA CHIEF', 27, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Lamar', N'GROUNDSKEEPER', 5, 2080, CAST(11.6200 AS Decimal(18, 4)), CAST(17.6800 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Lamar', N'HUMAN RESOURCES GENERALIST', 10, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Lamar', N'INVESTIGATOR ', 10, 2496, CAST(15.3000 AS Decimal(18, 4)), CAST(23.2800 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Lamar', N'INVESTIGATOR, SGT', 11, 2496, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Lamar', N'JAIL ADMINISTRATION', 12, 2496, CAST(20.1500 AS Decimal(18, 4)), CAST(30.3600 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Lamar', N'LABORER  ', 5, 2080, CAST(11.6200 AS Decimal(18, 4)), CAST(17.6800 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Lamar', N'LEAD PARAMEDIC', 14, 2496, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Lamar', N'OPERATOR ', 9, 2080, CAST(12.4500 AS Decimal(18, 4)), CAST(22.5000 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Lamar', N'PARAMEDIC', 12, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Lamar', N'PARKS & RECREATION DIRECTOR', 23, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Lamar', N'REC COORDINATOR', 6, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Lamar', N'REC FACILITIES ASSISTANT ', 5, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Lamar', N'REC FACILITIES TEAM LEADER ', 7, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Lamar', N'REGISTRAR', 6, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Lamar', N'ROAD CREW LEADER', 11, 2080, CAST(12.4500 AS Decimal(18, 4)), CAST(18.9400 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Lamar', N'ROAD SUPERINTENDENT', 27, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Lamar', N'ZONING ADMINISTRATOR ', 9, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pike', N'ACCOUNTING CLERK ', 9, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pike', N'ADMIN ASSISTANT ', 5, 2080, CAST(12.0000 AS Decimal(18, 4)), CAST(18.0000 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pike', N'AP CLERK', 9, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pike', N'APPRAISER ', 6, 2080, CAST(14.2500 AS Decimal(18, 4)), CAST(20.0000 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pike', N'CAPTAIN ADMIN ', 13, 2496, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pike', N'CHIEF APPRAISER', 23, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pike', N'CHIEF DEPUTY CLERK ', 9, 2080, CAST(11.0000 AS Decimal(18, 4)), CAST(20.0000 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pike', N'CHIEF DEPUTY SHERIFF ', 26, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pike', N'CHIEF DEPUTY TAX COMMISSIONER', 9, 2080, CAST(14.5000 AS Decimal(18, 4)), CAST(25.0000 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pike', N'CLERK ', 5, 2080, CAST(11.0000 AS Decimal(18, 4)), CAST(16.0000 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pike', N'COMMUNICATIONS OFFICER ', 6, 2496, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pike', N'COURTS - CHIEF CLERK/PT MAGISTRATE', 17, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pike', N'CUSTODIAN', 5, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pike', N'DEPUTY ', 9, 2496, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pike', N'DEPUTY CLERK', 6, 2496, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pike', N'DEPUTY FIRE CHIEF', 10, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pike', N'DEPUTY, LIEUTENANT', 12, 2496, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pike', N'DEPUTY, SGT', 11, 2496, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pike', N'DETENTION OFFICER  ', 6, 2496, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pike', N'DETENTION OFFICER SGT', 8, 2496, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pike', N'DETENTION OFFICER, CORP', 7, 2496, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pike', N'DETENTION OFFICER, LT', 10, 2496, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pike', N'ELECTIONS SUPERVISOR ', 9, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pike', N'EMERGENCY MEDCIAL TECHNICIAN (EMT)', 6, 2496, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pike', N'EMS DIRECTOR', 26, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pike', N'FACILITIES DIRECTOR', 26, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pike', N'FACILITIES MAINTENANCE TECHNICIAN', 16, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pike', N'FIRE/EMA CHIEF', 27, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pike', N'GROUNDSKEEPER', 5, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pike', N'HUMAN RESOURCES GENERALIST', 10, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pike', N'INVESTIGATOR ', 10, 2496, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pike', N'INVESTIGATOR, SGT', 11, 2496, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pike', N'JAIL ADMINISTRATION', 12, 2496, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pike', N'LABORER  ', 5, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pike', N'LEAD PARAMEDIC', 14, 2496, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pike', N'OPERATOR ', 9, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pike', N'PARAMEDIC', 12, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pike', N'PARKS & RECREATION DIRECTOR', 23, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pike', N'REC COORDINATOR', 6, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pike', N'REC FACILITIES ASSISTANT ', 5, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pike', N'REC FACILITIES TEAM LEADER ', 7, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pike', N'REGISTRAR', 6, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pike', N'ROAD CREW LEADER', 11, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pike', N'ROAD SUPERINTENDENT', 27, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Pike', N'ZONING ADMINISTRATOR ', 9, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Ware', N'ACCOUNTING CLERK ', 9, 2080, CAST(12.4400 AS Decimal(18, 4)), CAST(24.5300 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Ware', N'ADMIN ASSISTANT ', 5, 2080, CAST(10.4800 AS Decimal(18, 4)), CAST(18.4500 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Ware', N'AP CLERK', 9, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Ware', N'APPRAISER ', 6, 2080, CAST(13.3600 AS Decimal(18, 4)), CAST(21.2400 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Ware', N'CAPTAIN ADMIN ', 13, 2496, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Ware', N'CHIEF APPRAISER', 23, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Ware', N'CHIEF DEPUTY CLERK ', 9, 2080, CAST(11.8400 AS Decimal(18, 4)), CAST(18.8800 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Ware', N'CHIEF DEPUTY SHERIFF ', 26, 2080, CAST(38.0800 AS Decimal(18, 4)), CAST(38.0800 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Ware', N'CHIEF DEPUTY TAX COMMISSIONER', 9, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Ware', N'CLERK ', 5, 2080, CAST(12.1300 AS Decimal(18, 4)), CAST(19.3600 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Ware', N'COMMUNICATIONS OFFICER ', 6, 2496, CAST(11.6000 AS Decimal(18, 4)), CAST(20.9000 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Ware', N'COURTS - CHIEF CLERK/PT MAGISTRATE', 17, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Ware', N'CUSTODIAN', 5, 2080, CAST(8.8300 AS Decimal(18, 4)), CAST(12.2200 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Ware', N'DEPUTY ', 9, 2496, CAST(16.7400 AS Decimal(18, 4)), CAST(21.4200 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Ware', N'DEPUTY CLERK', 6, 2496, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Ware', N'DEPUTY FIRE CHIEF', 10, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Ware', N'DEPUTY, LIEUTENANT', 12, 2496, CAST(25.1400 AS Decimal(18, 4)), CAST(27.7300 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Ware', N'DEPUTY, SGT', 11, 2496, CAST(17.9400 AS Decimal(18, 4)), CAST(22.9300 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Ware', N'DETENTION OFFICER  ', 6, 2496, CAST(13.7600 AS Decimal(18, 4)), CAST(20.8700 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Ware', N'DETENTION OFFICER SGT', 8, 2496, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Ware', N'DETENTION OFFICER, CORP', 7, 2496, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Ware', N'DETENTION OFFICER, LT', 10, 2496, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Ware', N'ELECTIONS SUPERVISOR ', 9, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Ware', N'EMERGENCY MEDCIAL TECHNICIAN (EMT)', 6, 2496, CAST(12.1900 AS Decimal(18, 4)), CAST(21.4500 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Ware', N'EMS DIRECTOR', 26, 2080, CAST(25.1500 AS Decimal(18, 4)), CAST(25.6500 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Ware', N'FACILITIES DIRECTOR', 26, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Ware', N'FACILITIES MAINTENANCE TECHNICIAN', 16, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Ware', N'FIRE/EMA CHIEF', 27, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Ware', N'GROUNDSKEEPER', 5, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Ware', N'HUMAN RESOURCES GENERALIST', 10, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Ware', N'INVESTIGATOR ', 10, 2496, CAST(17.9500 AS Decimal(18, 4)), CAST(22.3700 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Ware', N'INVESTIGATOR, SGT', 11, 2496, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Ware', N'JAIL ADMINISTRATION', 12, 2496, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Ware', N'LABORER  ', 5, 2080, CAST(14.4800 AS Decimal(18, 4)), CAST(14.4800 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Ware', N'LEAD PARAMEDIC', 14, 2496, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Ware', N'OPERATOR ', 9, 2080, CAST(13.4700 AS Decimal(18, 4)), CAST(19.8400 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Ware', N'PARAMEDIC', 12, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Ware', N'PARKS & RECREATION DIRECTOR', 23, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Ware', N'REC COORDINATOR', 6, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Ware', N'REC FACILITIES ASSISTANT ', 5, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Ware', N'REC FACILITIES TEAM LEADER ', 7, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Ware', N'REGISTRAR', 6, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Ware', N'ROAD CREW LEADER', 11, 2080, CAST(24.0600 AS Decimal(18, 4)), CAST(24.5400 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Ware', N'ROAD SUPERINTENDENT', 27, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Ware', N'ZONING ADMINISTRATOR ', 9, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Washington', N'ACCOUNTING CLERK ', 9, 2080, CAST(16.8300 AS Decimal(18, 4)), CAST(28.8500 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Washington', N'ADMIN ASSISTANT ', 5, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Washington', N'AP CLERK', 9, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Washington', N'APPRAISER ', 6, 2080, CAST(12.0000 AS Decimal(18, 4)), CAST(25.0000 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Washington', N'CAPTAIN ADMIN ', 13, 2496, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Washington', N'CHIEF APPRAISER', 23, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Washington', N'CHIEF DEPUTY CLERK ', 9, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Washington', N'CHIEF DEPUTY SHERIFF ', 26, 2080, CAST(21.0000 AS Decimal(18, 4)), CAST(35.0000 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Washington', N'CHIEF DEPUTY TAX COMMISSIONER', 9, 2080, CAST(16.8300 AS Decimal(18, 4)), CAST(24.0400 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Washington', N'CLERK ', 5, 2080, CAST(10.0000 AS Decimal(18, 4)), CAST(20.0000 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Washington', N'COMMUNICATIONS OFFICER ', 6, 2496, CAST(13.0000 AS Decimal(18, 4)), CAST(18.0000 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Washington', N'COURTS - CHIEF CLERK/PT MAGISTRATE', 17, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Washington', N'CUSTODIAN', 5, 2080, CAST(10.0000 AS Decimal(18, 4)), CAST(20.0000 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Washington', N'DEPUTY ', 9, 2496, CAST(15.0000 AS Decimal(18, 4)), CAST(20.0000 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Washington', N'DEPUTY CLERK', 6, 2496, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Washington', N'DEPUTY FIRE CHIEF', 10, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Washington', N'DEPUTY, LIEUTENANT', 12, 2496, CAST(18.0000 AS Decimal(18, 4)), CAST(28.0000 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Washington', N'DEPUTY, SGT', 11, 2496, CAST(17.0000 AS Decimal(18, 4)), CAST(22.0000 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Washington', N'DETENTION OFFICER  ', 6, 2496, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Washington', N'DETENTION OFFICER SGT', 8, 2496, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Washington', N'DETENTION OFFICER, CORP', 7, 2496, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Washington', N'DETENTION OFFICER, LT', 10, 2496, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Washington', N'ELECTIONS SUPERVISOR ', 9, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Washington', N'EMERGENCY MEDCIAL TECHNICIAN (EMT)', 6, 2496, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Washington', N'EMS DIRECTOR', 26, 2080, CAST(19.2300 AS Decimal(18, 4)), CAST(28.8500 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Washington', N'FACILITIES DIRECTOR', 26, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Washington', N'FACILITIES MAINTENANCE TECHNICIAN', 16, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Washington', N'FIRE/EMA CHIEF', 27, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Washington', N'GROUNDSKEEPER', 5, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Washington', N'HUMAN RESOURCES GENERALIST', 10, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Washington', N'INVESTIGATOR ', 10, 2496, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Washington', N'INVESTIGATOR, SGT', 11, 2496, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Washington', N'JAIL ADMINISTRATION', 12, 2496, CAST(20.0000 AS Decimal(18, 4)), CAST(30.0000 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Washington', N'LABORER  ', 5, 2080, CAST(11.0000 AS Decimal(18, 4)), CAST(16.0000 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Washington', N'LEAD PARAMEDIC', 14, 2496, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Washington', N'OPERATOR ', 9, 2080, CAST(13.0000 AS Decimal(18, 4)), CAST(25.0000 AS Decimal(18, 4)), NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Washington', N'PARAMEDIC', 12, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Washington', N'PARKS & RECREATION DIRECTOR', 23, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Washington', N'REC COORDINATOR', 6, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Washington', N'REC FACILITIES ASSISTANT ', 5, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Washington', N'REC FACILITIES TEAM LEADER ', 7, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Washington', N'REGISTRAR', 6, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Washington', N'ROAD CREW LEADER', 11, 2080, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Washington', N'ROAD SUPERINTENDENT', 27, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[CustomerImport] ([locnam], [posnam], [posgrd], [poshrs], [minhrs], [maxhrs], [minsal], [maxsal]) VALUES (N'Washington', N'ZONING ADMINISTRATOR ', 9, 2080, NULL, NULL, NULL, NULL)
GO
