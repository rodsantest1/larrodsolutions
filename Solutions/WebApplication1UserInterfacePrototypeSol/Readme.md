

# Excel Tabs to Understand First
1. Proposed Matrix
2. Current Vs Proposed
3. Employee Data

## Proposed Matrix - You only need 2 values to populate the entire table
1. Starting Hourly Salary Pay
2. Base Minimum Salary for base pay grade



Database1 Prep for Excel OpenRowSet

```sql
sp_configure 'show advanced options', 1;  
RECONFIGURE;
GO 
sp_configure 'Ad Hoc Distributed Queries', 1;  
RECONFIGURE;  
GO  
```

Project Hours:
9/7 - 21/22
Converting Excel calculations to SQL queries
15-18 hr Analysis, Design, Development and Testing  

9/23/22
2hr - Working on Employee Data Tab in Excel

9/24/22
5hr - Working on Employee Data Tab in Excel

9/28/22
2hr - Working on dashboard stored procedures and testing

10/1/22
1hr - implementing months remaining (analysis, design, dev, and testing)



- A Nice Touch [[Go](https://stackoverflow.com/q/73788920/139698)]  
- GitLab Tutorial [[Go]](https://youtu.be/_4SmIyQ5eis)  
- Table Value Parameter [[Go](https://docs.microsoft.com/en-us/sql/relational-databases/tables/use-table-valued-parameters-database-engine?view=sql-server-ver16)]  
- Complete CI/CD for C# and SQL Server by Tim Corey [[Go](https://youtu.be/8-mqy-um6rw)]  
- Microsoft DevBox new [[Go](https://learn.microsoft.com/en-us/azure/dev-box/quickstart-configure-dev-box-service?tabs=AzureADJoin#create-a-dev-center)]  
- Pivot and Unpivot [[Go](https://learn.microsoft.com/en-us/sql/t-sql/queries/from-using-pivot-and-unpivot?view=sql-server-ver16)]  