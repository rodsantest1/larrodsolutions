﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EECDemo
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            GetDashboard();

            if (!IsPostBack)
            {
                TxtStartingSalary.Text = (string)Cache["salary"] ?? "15.00";
                DdlCounty.Text = (string)Cache["county"] ?? "Pierce";
                TxtMonthsRemaining.Text = (string)Cache["monthsRemaining"] ?? "8";
                Cache["county"] = DdlCounty.Text;
                Cache["salary"] = TxtStartingSalary.Text;
                Cache["monthsRemaining"] = TxtMonthsRemaining.Text;
            }
            else
            {
                Cache["county"] = DdlCounty.Text;
                Cache["salary"] = TxtStartingSalary.Text;
                Cache["monthsRemaining"] = TxtMonthsRemaining.Text;
            }

        }

        private void GetDashboard()
        {
            DataSet ds = (DataSet)Cache["CPSData"];

            if (null == ds)
            {
                BusinessLayer bll = new BusinessLayer();
                decimal.TryParse(TxtStartingSalary.Text, out decimal startingSalary);
                int.TryParse(TxtMonthsRemaining.Text, out int monthsRemaining);
                ds = bll.GetDashboard(DdlCounty.Text, startingSalary, monthsRemaining);

                Cache["CPSData"] = ds;
            }
        }

        protected void BtnGetDashboard_Click(object sender, EventArgs e)
        {
            Cache.Remove("CPSData");
            GetDashboard();
            Label1.Text = "Loaded";
        }

        protected void DdlCounty_SelectedIndexChanged(object sender, EventArgs e)
        {
            Cache.Remove("CPSData");
            GetDashboard();
            Label1.Text = "Loaded";
        }
    }
}