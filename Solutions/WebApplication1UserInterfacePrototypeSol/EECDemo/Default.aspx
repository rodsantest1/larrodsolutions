﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="EECDemo.WebForm1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1 style="margin: 0px;">Welcome to CPS</h1>
    <h2 style="margin: 0px 0px 20px 0px;">by EEC</h2>

    <div style="margin: 5px; font-size:x-large">
        <asp:Label ID="Label2" runat="server" Text="County"></asp:Label>
        <asp:DropDownList style="font-size:x-large" ID="DdlCounty" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DdlCounty_SelectedIndexChanged">
            <asp:ListItem Text="Pierce" />
            <asp:ListItem Text="Brantley" />
            <asp:ListItem Text="Elbert" />
            <asp:ListItem Text="Appling" />
            <asp:ListItem Text="Berrien" />
            <asp:ListItem Text="Dodge" />
            <asp:ListItem Text="Lamar" />
            <asp:ListItem Text="Pike" />
            <asp:ListItem Text="Ware" />
            <asp:ListItem Text="Washington" />
        </asp:DropDownList>
    </div>

    <div style="margin:5px; font-size:x-large">
        <asp:Label ID="Label3" runat="server" Text="Starting Salary"></asp:Label>
        <asp:TextBox style="font-size:x-large" Width="150" ID="TxtStartingSalary" Text="15.00" runat="server"></asp:TextBox>
    </div>

    <div style="margin:5px; font-size:x-large">
        <asp:Label ID="Label6" runat="server" Text="Months Remaining"></asp:Label>
        <asp:TextBox style="font-size:x-large;" Width="50" ID="TxtMonthsRemaining" Text="8" runat="server"></asp:TextBox>
    </div>
    <div style="margin:5px;">
        <asp:Button style="font-size:x-large; background-color: lightskyblue; border-radius: 6px;" ID="BtnGetDashboard" runat="server" Text="Update Dashboard" OnClick="BtnGetDashboard_Click" />
        <asp:GridView ID="GridView1" runat="server"></asp:GridView>
    </div>
    <asp:Label ID="Label1" runat="server"></asp:Label>

    <div style="margin:5px;">
        <p style="color: red;">
            NOTE: There is still an issue with positions and job codes discrepancies, so the totals will be off a bit in certain places. 
        </p>
    </div>

    <div style="margin:5px;">
        <p>
            When you select a county, that county will be excluded from the averaging.
        </p>
    </div>

</asp:Content>
