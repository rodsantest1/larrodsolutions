﻿using System;
using System.Collections.Generic;
using System.Data;
using DataLayer;

public class BusinessLayer
{

    internal DataSet GetDashboard(string county, decimal startingSalary, int monthsRemaining)
    {
        var dal = new DataAccess();
        var dashboard = dal.GetDashBoard(county, startingSalary, monthsRemaining);

        return dashboard;
    }

}