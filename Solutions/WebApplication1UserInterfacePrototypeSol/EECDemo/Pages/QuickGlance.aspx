﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="QuickGlance.aspx.cs" Inherits="EECDemo.QuickGlance" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        td:nth-child(3n) {
            text-align: right;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>Quick Glance for <%= (string)Cache["county"] %></h1>

    <div>
        <asp:GridView ID="GridView1" runat="server"></asp:GridView>
    </div>
</asp:Content>
