﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EECDemo
{
    public partial class EmployeeData : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            GetDashboard();
        }

        private void GetDashboard()
        {
            DataSet ds = (DataSet)Cache["CPSData"];

            if (ds == null)
            {
                Response.Redirect("~/");
            }

            GridView1.DataSource = ds.Tables["EmployeeData"];
            GridView1.DataBind();
        }
    }
}