﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="CurrentVsProposed.aspx.cs" Inherits="EECDemo.CurrentVsProposed" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        td {
            text-align: right;
        }

        td:nth-child(2) {
            text-align: left;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>Current Vs Proposed for <%= (string)Cache["county"] %></h1>

    <div>
        <asp:GridView ID="GridView1" runat="server"></asp:GridView>
    </div>    
</asp:Content>
