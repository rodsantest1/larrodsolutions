﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="EmployeeData.aspx.cs" Inherits="EECDemo.EmployeeData" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>Employee Data for <%= (string)Cache["county"] %></h1>

    <div>
        <asp:GridView ID="GridView1" runat="server"></asp:GridView>
    </div>
    <script>
        $(function () {
            $('td:contains(".")').css('text-align', 'right');
        });
    </script>
</asp:Content>
