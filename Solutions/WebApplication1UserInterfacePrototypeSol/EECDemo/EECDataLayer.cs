﻿namespace DataLayer
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Threading.Tasks;

    public class DataAccess
    {

        String connString;
        public DataAccess()
        {
            connString = ConfigurationManager.ConnectionStrings["testdb"].ConnectionString;
        }

        public DataSet GetDashBoard(string county, decimal startingSalary, int monthsRemaing)
        {
            using (var con = new SqlConnection(connString))
            {
                con.Open();
                var query = @"GetDashboard";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.Parameters.AddWithValue("county", county);
                cmd.Parameters.AddWithValue("startingSalary", startingSalary);
                cmd.Parameters.AddWithValue("monthsRemaining", monthsRemaing);
                cmd.CommandType = CommandType.StoredProcedure;
                var sqlAdapter = new SqlDataAdapter(cmd);
                var ds = new DataSet();
                sqlAdapter.Fill(ds);
                ds.Tables[0].TableName = "ProposedMatrix";
                ds.Tables[1].TableName = "CurrentVsProposed";
                ds.Tables[2].TableName = "EmployeeData";
                ds.Tables[3].TableName = "QuickGlance";

                return ds;
            }
        }

    }
}